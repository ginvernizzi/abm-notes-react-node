
describe('Notes app', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000')
    cy.request('post', 'http://localhost:3001/api/testing/reset')

    const user = {
      username: "gsantacruz",
      user: "gonzalo",
      password: "12345678"
    }

    cy.request('post', 'http://localhost:3001/api/users', user)

  })

  it('frontpage can be opened', () => {
    cy.contains('Notes')
  })

  it('user can login ', () => {
    cy.contains('Login')
    cy.get('[name="username"]').type('gsantacruz')
    cy.get('[name="password"]').type('12345678')
    cy.contains('Login').click()
    cy.contains('Create a new note')
  })

  it('Login fails wrong password', () => {
    cy.contains('Login')
    cy.get('[name="username"]').type('gsantacruz')
    cy.get('[name="password"]').type('12345679')
    cy.contains('Login').click()

    // cy.contains('user or password incorrect')
    cy.get('.error').should('contain', 'user or password incorrect')
  })

  describe('when logged in', () => {
    beforeEach(() => {
      cy.contains('Login')
      cy.get('[name="username"]').type('gsantacruz')
      cy.get('[name="password"]').type('12345678')
      cy.contains('Login').click()
      cy.contains('Create a new note')
    })

    it('can create a new note', () => {
      const content = 'nueva notita de test'
      cy.get('[name="text_note"]').type(content)
      cy.contains('save').click()
      cy.contains(content)
    })

    describe('a note exists', () => {
      beforeEach(() => {
        cy.createNote({
          content: 'primera nota',
          important: false
        })

        cy.createNote({
          content: 'segunda nota',
          important: false
        })

        cy.createNote({
          content: 'tercera nota',
          important: false
        })
      })

      it('and can be made important', () => {
        cy.contains('segunda nota').as('theNote')
        
        cy.get('@theNote')
        .contains('make important')
        .click()

        cy.get('@theNote')
        .contains('make not important')        
      })
    })
  })
})