/* eslint-disable testing-library/render-result-naming-convention */
/* eslint-disable testing-library/prefer-screen-queries */
import React, { Component } from "react";
import '@testing-library/jest-dom/extend-expect'
import { fireEvent, render } from '@testing-library/react'
import Note from "./Note";

describe("componente nota", () => {
  test('render content', () => {
    const note = {
      content: 'this is a test',
      important: true
    }
  
    const component = render(<Note note={note}/> )  
    const li = component.container.querySelector('li')
    component.getByText('this is a test')
    
    expect(component.container).toHaveTextContent('this is a test')    
    // console.log(prettyDOM(li))
    
  })

  test('count click on -toggle importance- button', () => { 
const note = {
      content: 'this is a test',
      important: true
    }

    const mockHandler = jest.fn()
  
    const component = render(<Note note={note} toggleImportance={mockHandler}/> )
    const button = component.getByText('make not important')  
    fireEvent.click(button)
    fireEvent.click(button)

    expect(mockHandler.mock.calls).toHaveLength(2)
    
   })
})
