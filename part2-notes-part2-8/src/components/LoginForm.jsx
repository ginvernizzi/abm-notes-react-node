import React, { useState } from "react";
import Notification from "./Notification";
import { useNavigate } from "react-router-dom";
import useUser from "../hooks/useUser";
import useField from "../hooks/useField";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import FormGroup from "react-bootstrap/FormGroup";

import Button from "react-bootstrap/Button";

const LoginForm = () => {
  const usernameField = useField({ type: "text" }); // value, type, onHandleChange
  const passwordField = useField({ type: "password" });

  const navigate = useNavigate();
  const { user, login, logout } = useUser();
  const [errorMessage, setErrorMessage] = useState("");

  const onHandleLogin = async (e) => {
    e.preventDefault();

    try {
      if (usernameField.value && passwordField.value) {
        await login(usernameField.value, passwordField.value);
        navigate("/notes");
      } else {
        setErrorMessage("Usuario o password no debe estar vacio");
      }
    } catch (error) {
      setErrorMessage(await error.message);
      // console.log(error)
      setInterval(() => {
        setErrorMessage(null);
      }, 10000);
    }
  };

  const onHandleLogout = (e) => {
    logout();
  };

  if (user !== null) {
    return (
      <div>
        {user.username} is logged{" "}
        <button onClick={(e) => onHandleLogout(e)}> Log out </button>
      </div>
    );
  }

  return (
    <>
      {errorMessage && <Notification message={errorMessage}></Notification>}
      <h2>Bienvenido a app de notas</h2>
      <Form onSubmit={(e) => onHandleLogin(e)}>
        <FormGroup>
          <FormControl
            placeholder="Ingrese usuario"
            name="username"
            {...usernameField}
          />

          <FormControl
            placeholder="Ingrese password"
            name="password"
            {...passwordField}
          />
          <Button type="submit">Login</Button>
        </FormGroup>
      </Form>
    </>
  );
};

export default LoginForm;
