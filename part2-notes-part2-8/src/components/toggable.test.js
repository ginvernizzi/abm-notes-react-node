/* eslint-disable testing-library/prefer-screen-queries */
/* eslint-disable testing-library/no-render-in-setup */
import React from "react";
import '@testing-library/jest-dom/extend-expect'
import { fireEvent, prettyDOM, render } from '@testing-library/react'
import Toggable from "./Toggable";

let component   

beforeEach(() => {
  component = render(<Toggable > <div>testDivContent</div> </Toggable>)
})

test('render its children', () => { 
  component.getByText('testDivContent')
})

test('should first', () => { 
  const el = component.getByText('testDivContent')
  console.log(prettyDOM(component.container)) 
  expect(el.parentNode).toHaveStyle('display:none')
})