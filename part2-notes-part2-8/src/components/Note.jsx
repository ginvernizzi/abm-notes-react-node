import React from "react";
import { Link } from "react-router-dom";

const Note = ({ note, toggleImportance }) => {
  const label = note.important ? "make not important" : "make important";

  return (
    <>
      <th>
        <Link to={`/notes/${note.id}`}> {note.content} </Link>
      </th>
      <th>
        <button onClick={toggleImportance}>{label}</button>
      </th>
    </>
  );
};

export default Note;
