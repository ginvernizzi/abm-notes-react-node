import React, { useState } from "react";
import noteService from "../services/notes";

const NoteForm = ({ setNotes }) => {
  const [content, setContent] = useState("");

  const addNote = (event) => {
    event.preventDefault();
    const noteObject = {
      content: content,
      important: Math.random() > 0.5,
    };

    noteService
      .create(noteObject)
      .then((returnedNote) => {
        setNotes((old_notes) => {
          return [...old_notes, returnedNote]
        });
      })
      .catch((error) => console.log(error));

    setContent("");
  };

  const handleNoteChange = (event) => {
    setContent(event.target.value);
  };

  return (
    <>
      <div>Create a new note</div>
      <form data-test-id="login-form" onSubmit={addNote}>
        <input
          name="text_note"
          value={content}
          onChange={(event) => handleNoteChange(event)}
        />
        <button type="submit">save</button>
      </form>
    </>
  );
};

export default NoteForm;
