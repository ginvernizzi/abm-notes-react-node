import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import noteService  from "../services/notes";


const NoteDetails = () => {
  const { id } = useParams();
  const [notes, setNotes] = useState ([]);
  const note = notes.find((note) => note.id === id);
  
  useEffect(() => {
    console.log("seeeeeeeeeeee")
    noteService.getAll().then((notes) => {
      setNotes(notes);
    });
  }, []);


  return (
    <>
      <div> Note Details: </div>
      <h2>{note.content}</h2> */}
      {<p>{note.important ? "importante" : "no importa"}</p>}
    </>
  );
};

export default NoteDetails;
