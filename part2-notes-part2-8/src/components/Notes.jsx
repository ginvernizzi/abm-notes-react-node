import React, { useState } from "react";
import { Link } from "react-router-dom";
import useUser from "../hooks/useUser";
import useNotes from "../hooks/useNotes";
import Note from "./Note";
import Notification from "./Notification";
import Footer from "./Footer";
import noteService from "../services/notes";
import NoteForm from "./NoteForm";
import Table from "react-bootstrap/Table";

const Notes = () => {
  const [newNote, setNewNote] = useState("");
  const [showAll, setShowAll] = useState(true);
  const { notes, setNotes } = useNotes();
  const [errorMessage, setErrorMessage] = useState("");
  const { user } = useUser();

  const addNote = (event) => {
    event.preventDefault();
    const noteObject = {
      content: newNote,
      important: Math.random() > 0.5,
    };

    noteService.create(noteObject).then((returnedNote) => {
      setNotes(notes.concat(returnedNote));
      setNewNote("");
    });
  };

  const handleNoteChange = (event) => {
    setNewNote(event.target.value);
  };

  const toggleImportanceOf = (id) => {
    const note = notes.find((n) => n.id === id);
    const changedNote = { ...note, important: !note.important };

    noteService
      .update(id, changedNote)
      .then((returnedNote) => {
        setNotes(notes.map((note) => (note.id !== id ? note : returnedNote)));
      })
      .catch((error) => {
        setErrorMessage(
          `Note '${note.content}' was already removed from server`
        );
        setTimeout(() => {
          setErrorMessage(null);
        }, 5000);
        setNotes(notes.filter((n) => n.id !== id));
      });
  };

  const notesToShow = showAll ? notes : notes.filter((note) => note.important);

  return (
    <div>
      <h1>Notes</h1>

      {user == null ? (
        <div>
          <p>
            {" "}
            Para agregar notas debe loguearse primero{" "}
            <Link to="/login"> Login </Link>{" "}
          </p>
        </div>
      ) : (
        <div>
          <div>
            <NoteForm
              addNote={addNote}
              newNote={newNote}
              handleNoteChange={handleNoteChange}
            ></NoteForm>
          </div>
        </div>
      )}

      {errorMessage && <Notification message={errorMessage} />}
      <div>
        <button onClick={() => setShowAll(!showAll)}>
          show {showAll ? "important" : "all"}
        </button>
      </div>
      <Table striped bordered hover>
        {notesToShow.map((note) => (
          <tbody key={note.id}>
            <tr>
                <Note
                  note={note}
                  toggleImportance={() => toggleImportanceOf(note.id)}
                />
            </tr>
          </tbody>
        ))}
      </Table>
      <Footer />
    </div>
  );
};

export default Notes;
