import React from 'react'
import { useState } from 'react'

const Toggable = ({ children }) => {
  const [visibility, setVisibility] = useState(true)

  const showElement = { display: visibility ? "" : "none" }
  const hideElement = { display: visibility ? "none" : "" }

  const toggable = (e) => {
    setVisibility(!visibility)
  }

  return (
    <div>
      <div style={showElement}>
        {children}
        <button onClick={(e) => toggable(e)}> Ocultar </button>
      </div>
      <div style={hideElement}>
        <button onClick={(e) => toggable(e)}> Mostrar  </button>
      </div>
    </div>
  )
}

export default Toggable