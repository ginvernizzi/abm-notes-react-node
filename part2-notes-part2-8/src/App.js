import React from "react";
import { BrowserRouter, Link, Routes, Route } from "react-router-dom";
import Notes from "./components/Notes";
import NoteDetails from "./components/NoteDetails";
import LoginForm from "./components/LoginForm";
import useUser from "./hooks/useUser";
import NavLink from "react-bootstrap/NavLink";
import Navbar from "react-bootstrap/Navbar";
import NavbarToggle from "react-bootstrap/NavbarToggle";
import Nav from "react-bootstrap/Nav";
import NavbarCollapse from "react-bootstrap/NavbarCollapse";

const App = () => {
  const { user } = useUser();

  const Home = () => <h1> Home </h1>;

  return (
    <BrowserRouter>
      <div className="container">
        <Navbar collapseOnSelect bg="light" expand="lg">
          <NavbarToggle aria-controls="responsive-navbar-nav" />

          <NavbarCollapse>
            <Nav>
              <NavLink>
                {" "}
                <Link to="/">Home</Link>{" "}
              </NavLink>
              <NavLink>
                {" "}
                <Link to="/notes"> Notes </Link>{" "}
              </NavLink>
              {!user && (
                <NavLink>
                  {" "}
                  <Link to="/login">Login</Link>{" "}
                </NavLink>
              )}
            </Nav>
          </NavbarCollapse>
        </Navbar>

        <Routes>
          <Route path={"/"} element={<Home></Home>} />
          <Route path={"/notes"} element={<Notes></Notes>} />
          <Route path={"/login"} element={<LoginForm></LoginForm>} />
          <Route path={"/notes/:id"} element={<NoteDetails />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
};

export default App;
