import {useState, useEffect} from 'react'
import noteService from '../services/notes'

const useNotes = () => {
    const [notes, setNotes] = useState([]);
    useEffect(() => {
      noteService.getAll().then((initialNotes) => {
        setNotes(initialNotes);
      });
    }, []);

    return {notes, setNotes}
}

export default useNotes