import { useState, useEffect } from "react";
import noteService from "../services/notes";
import loginService from "../services/login";

const useUser = () => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const loggedUserJSON = window.localStorage.getItem("loggedNoteappUser");
    if (loggedUserJSON) {
      const user = JSON.parse(loggedUserJSON);
      setUser(user);
      noteService.setToken(user.token);
    }
  }, []);

  const login = async (username, password) => {
  debugger
   const user = await loginService.login( { username, password });
    window.localStorage.setItem("loggedNoteappUser", JSON.stringify(user));
    noteService.setToken(user.token);
    setUser(user);
    // usernameField.setValue("");
    // passwordField.setValue("");
  };

  const logout = () => {
    window.localStorage.removeItem("loggedNoteappUser");
    setUser(null);
    noteService.setToken("");
  };

  return { user, setUser, login, logout };
};

export default useUser;
