import { useState } from "react";

const useError = (message) => {
  const [errorMessage, setErrorMessage] = useState(null);

  setErrorMessage(message);

  return { errorMessage, setErrorMessage };
};

export default useError;
