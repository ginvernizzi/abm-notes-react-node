const testingRouter = require('express').Router()

const note = require('../models/note')
const user = require('../models/user')

testingRouter.post('/reset', async (request, response) => {
  await note.deleteMany({})
  await user.deleteMany({})

  response.status(204).end()
})

module.exports = testingRouter