const bcrypt = require('bcrypt')
const usersRouter = require('express').Router()
const User = require('../models/user')


usersRouter.post('/', async (request, response) => {
  const { username, name, password } = request.body

  const saltRounds = 10
  const passwordHash = await bcrypt.hash(password, saltRounds)

  const user = new User({
    username,
    name,
    passwordHash,
  })

  const savedUser = await user.save()
  response.json(savedUser)
})

usersRouter.get('/', async (request, response) => {
  const usersDb = await User.find({}).populate('notes', { content:1 })
  if (usersDb) {
    response.json(usersDb)
  } else {
    response.status(501)
  }
})

usersRouter.patch('/', async (request, response) => {
  const { username, password } = request.body
  const userDB = await User.findOne({ username })

  userDB.passwordHash = await bcrypt.hash(password, 10)
  await userDB.save()

  response.status(200).json({ message: 'usuario modificado' })
})


usersRouter.delete('/:id', async (request, response) => {
  const { id } = request.params
  await User.findByIdAndDelete(id)
  response.status(204)
})

module.exports = usersRouter