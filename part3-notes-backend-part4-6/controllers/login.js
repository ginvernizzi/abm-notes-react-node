const jwt = require('jsonwebtoken')
const loginRouter = require('express').Router()
const User = require('../models/user')
const bcrypt = require('bcrypt')

loginRouter.post('/', async(request, response) => {
  const { username, password } = request.body
  const userDb = await User.findOne({ username })
  if(!userDb){
    response.status(404).json({ error: 'user not authorized' })
  } else {
    if(await bcrypt.compare(password, userDb.passwordHash)){
      const userForToken = {
        id: userDb._id,
        username: username
      }
      const token = jwt.sign(userForToken, process.env.SECRET)
      console.log('token pa', token)

      response.status(200).send({
        name: userDb.name,
        username: username,
        token
      })
    }else{
      response.status(404).json({ error: 'user or password incorrect' })
    }
  }
})

module.exports = loginRouter