const notesRouter = require('express').Router()
const Note = require('../models/note')
const User = require('../models/user')
const userExtractor = require('../utils/userExtractor')

notesRouter.get('/', async (request, response) => {
  const notes = await Note.find({})
  response.json(notes)
})

notesRouter.post('/', userExtractor, async (request, response) => {
  const { body } = request

  const user = await User.findById(request.user_id)

  if(!user){
    return response.status(401).json({ error: 'usuario inexistente' })
  }

  const note = new Note({
    content: body.content,
    important: false,
    date: new Date(),
    user: user.id
  })

  const savedNote = await note.save()
  user.notes = user.notes.concat(savedNote.id)
  await user.save()

  response.status(201).json(savedNote)
})

notesRouter.get('/:id', async (request, response) => {
  const note = await Note.findById(request.params.id)
  if (note) {
    response.json(note.toJSON())
  } else {
    response.status(404).end()
  }
})


notesRouter.delete('/:id', userExtractor, async (request, response) => {
  await Note.findByIdAndRemove(request.params.id)
  response.status(204).end()
})

notesRouter.put('/:id', (request, response, next) => {
  const body = request.body

  const note = {
    content: body.content,
    important: body.important,
  }

  Note.findByIdAndUpdate(request.params.id, note, { new: true })
    .then(updatedNote => {
      response.json(updatedNote)
    })
    .catch(error => next(error))
})

module.exports = notesRouter