const jwt= require('jsonwebtoken')

module.exports = ((request, response, next) => {
  let token = null
  let decodedToken = {}
  const authorization = request.get('authorization')

  if(authorization && authorization.toLowerCase().startsWith('bearer')) {
    token = authorization.substring(7)
  }

  try {
    decodedToken = jwt.verify(token, process.env.SECRET)
  } catch (error) {
    console.log('error jwt', error)
  }

  if(!token || !decodedToken.id){
    return response.status(401).json({ error: 'token missing or invalid' })
  }

  request.user_id = decodedToken.id

  next()
})